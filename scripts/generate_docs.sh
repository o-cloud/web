#!/usr/bin/env bash
set -e


BASE_PATH=${1:?Please specify a docs directory}

base_DOCS="README.md"

# Name:weigth

declare -A MARKDOWNS=(
    ['Service Provider:11']="https://gitlab.com/o-cloud/service-provider/-/raw/main"
    ['Compute Provider:11']="https://gitlab.com/o-cloud/compute-provider/-/raw/main"
    ['Pipeline Orchestrator:11']="https://gitlab.com/o-cloud/pipeline-orchestrator/-/raw/main"
    ['Admin Frontend:11']="https://gitlab.com/o-cloud/admin-ihm/-/raw/main"
    ['Frontend:11']="https://gitlab.com/o-cloud/frontend/-/raw/main"
    ['Cluster Discovery:11']="https://gitlab.com/o-cloud/cluster-discovery/-/raw/main"
    ['Sentinel Provider:11']="https://gitlab.com/o-cloud/sentinel-provider/-/raw/main"
    ['Generic Backend:11']="https://gitlab.com/o-cloud/generic-backend/-/raw/main"
    ['SB Architecture:10']="https://gitlab.com/o-cloud/docs/sb-architecture/-/raw/main"
)

for k in "${!MARKDOWNS[@]}"; do
    weigth=${k#*:}
    name=${k%:*} # Get all before :

    k2=${name/ /-} # Replace space by -
    k2=${k2,,} # lowercase
    mkdir -p $BASE_PATH/$k2
    content=$(wget -q -O- "${MARKDOWNS[$k]}/$base_DOCS")
    
    DIR_PATH=$BASE_PATH/$k2

cat <<EOF > "$DIR_PATH/index.md"
---
title: "$name"
date: $(date -Iseconds)
draft: false
summary:
weight: $weigth
---

$content
EOF

# https://i.redd.it/k6sded6b9mkz.png
echo "$content" | \
 grep -Eo '!\[.*\]\(([./_a-zA-Z0-9\-]+)( \".*\")?\)' | \
 sed -r 's/!\[.*\]\(([./_a-zA-Z0-9\-]+)( \".*\")?\)/\1/' | \
 while read image; do
    echo "$name contains a image URI to $image"
    mkdir -p "$DIR_PATH/$(dirname $image)"
    wget -q -O "$DIR_PATH/$image" "${MARKDOWNS[$k]}/$image"
 done

done