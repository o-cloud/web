---
title: "Installation"
date: 2021-11-16T10:56:04+01:00
draft: true
summary: Install and use Open-Cloud
weight: 1
---

# Prerequisites

In order to use this system, you must have:

- Kubernetes cluster over Linux
- [Helm](https://helm.sh)

# Helm

The recommended way of using OpenCloud is through Helm.

```bash
helm repo add xxxxxxx
helm install bla bla bla
```
